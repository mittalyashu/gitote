# Git + Rem(ote) = Gitote ❤️

#### Software version control made simple!

Welcome to the [gitote](https://gitote.in) codebase. We are so excited to have you. With your help, we can build out Gitote to be more stable and better serve our platform.

#### Shields 🛡

Linux | Windows | Go Report | Donate | Slack
----- | ------- | --------- | ------ | -----
[![GitLab Build status](https://img.shields.io/gitlab/pipeline/gitote/gitote.svg?logo=gitlab)](https://gitlab.com/gitote/gitote/pipelines) | [![AppVeyor Build status](https://img.shields.io/appveyor/ci/yoginth/gitote.svg?logo=appveyor)](https://ci.appveyor.com/project/yoginth/gitote) | [![Go Report](https://goreportcard.com/badge/gitote.in/gitote/gitote)](https://goreportcard.com/report/gitote.in/gitote/gitote) | [![Liberapay](https://img.shields.io/liberapay/receives/gitote.svg?logo=liberapay&maxAge=3600)](https://liberapay.com/gitote/donate) | [![Slack](https://img.shields.io/badge/Slack-Join-brightgreen.svg?logo=slack&colorB=e01563)](https://gitote-slack.herokuapp.com)

#### Docker 🐬

Downloads | Quay | Azure
--------- | ---- | -----
[![Downloads](https://img.shields.io/docker/pulls/gitote/gitote.svg?logo=docker&maxAge=3600)](https://store.docker.com/community/images/gitote/gitote) | [![Quay](https://quay.io/repository/gitote/gitote/status)](https://quay.io/repository/gitote/gitote) | [![Build status](https://dev.azure.com/gitote/gitote/_apis/build/status/Gitote%20CI)](https://dev.azure.com/gitote/gitote/_build/latest?definitionId=1)

#### Versions 📦

GoLang | PostgreSQL | Git
------ | ---------- | ---
[![GoLang](https://img.shields.io/badge/GoLang-v1.7-green.svg?logo=go&maxAge=3600)](https://golang.org) | [![PostgreSQL](https://img.shields.io/badge/PostgreSQL-v9.5-green.svg?logo=postgresql&maxAge=3600)](https://www.postgresql.org) | [![Git](https://img.shields.io/badge/Git-v1.7.1-green.svg?logo=git&maxAge=3600)](https://git-scm.com/)

## What is gitote ❓

[Gitote](https://gitote.in) is an open source end-to-end software development platform with built-in version control, issue tracking, code review, and more.

## Table of Contents

- [Contributing](#contributing-)
    - [Where to contribute](#where-to-contribute)
    - [How to contribute](#how-to-contribute)
    - [Contribution guideline](#contribution-guideline-)
- [Codebase](#codebase-)
    - [The stack](#the-stack)
- [Documentation](#documentation-)
- [Features](#features-)
- [Core team](#core-team-)
- [Sponsors](#sponsors-)
- [Thanks](#thanks-)
- [License](#license-)

## Contributing 🚧

We expect contributors to abide by our underlying [code of conduct](CONDUCT.md). All conversations and discussions on Gitote (issues, pull requests) and across Gitote must be respectful and harassment-free.

<img align="right" src="https://i.imgsafe.org/61/611fe4067c.gif" width="200"/>

### Where to contribute

When in doubt, ask a [core team member](#core-team)! You can mention us in any issues . Any issue with [`Good first Issue`](https://gitote.in/gitote/gitote/issues?labels=9) tag is typically a good place to start.

✨ **Refactoring code**, e.g. improving the code without modifying the behavior is an area that can probably be done based on intuition and may not require much communication to be merged.

🐞 **Fixing bugs** may also not require a lot of communication, but the more the better. Please surround bug fixes with ample tests. Bugs are magnets for other bugs. Write tests near bugs!

🏗 **Building features** is the area which will require the most communication and/or negotiation. Every feature is subjective and open for debate. As always, when in doubt, ask!

### How to contribute

1.  Fork the project & clone locally. Follow the initial setup [here](docs/INSTALLATION.md).
2.  Create a branch, naming it either a feature or bug: `git checkout -b feature/that-new-feature` or `bug/fixing-that-bug`
3.  Code and commit your changes. Bonus points if you write a [good commit message](https://chris.beams.io/posts/git-commit/): `git commit -m 'Add some feature'`
4.  Push to the branch: `git push origin feature/that-new-feature`
5.  [Create a pull request](#create-a-pull-request) for your branch 🎉

## Contribution guideline 📜

### Create an issue

Nobody's perfect. Something doesn't work? Or could be done better? Let us know by creating an issue.

PS: a clear and detailed issue gets lots of love, all you have to do is follow the issue template!

### Clean code with tests

Some existing code may be poorly written or untested, so we must have more scrutiny going forward. We test with **go test**, let us know if you have any questions about this!

### Create a pull request

* Try to keep the pull requests small. A pull request should try its very best to address only a single concern.
* Make sure all tests pass and add additional tests for the code you submit.
* Document your reasoning behind the changes. Explain why you wrote the code in the way you did. The code should explain what it does.
* If there's an existing issue related to the pull request, reference to it by adding something like `References/Closes/Resolves #305`, where 305 is the issue number.
* If you follow the pull request template, you can't go wrong.

## Codebase 💻

### The stack

View **Gitote** tech stack [here](docs/STACK.md)

## Documentation 📑

View **Gitote** documentations [here](docs)

## Features 🔮

- Activity timeline
- **SSH** and **HTTP/HTTPS** protocols
- Account/Organization/Repository management
- Add/Remove repository collaborators
- Repository/Organization **webhooks** (including Slack and Discord)
- Repository **Git hooks/deploy keys**
- Repository **issues**, **pull requests**, **wiki** and **protected branches**
- **Migrate** and **mirror** repository and its wiki
- **Web editor** for repository files and wiki
- **Jupyter Notebook**
- **Two-factor authentication**
- **Gravatar** and **Federated avatar** with custom source
- **Mail** service
- More emojis. 🐈

## Core team 👬

- [@yoginth](https://gitote.in/yogi)

## Sponsors 🤗

<p>
    <a href="https://cloudflare.com/?utm_source=Gitote">
        <img src="https://cdn.gitote.in/img/sponsors/cloudflare.png" width="220"/>
    </a>
</p>

<p>
    <a href="https://m.do.co/c/1ddec8d50826">
        <img src="https://cdn.gitote.in/img/sponsors/digitalocean.png" width="220"/>
    </a>
</p>

<p>
    <a href="https://browserstack.com/?utm_source=Gitote">
        <img src="https://cdn.gitote.in/img/sponsors/browserstack.png" width="220"/>
    </a>
</p>

## Thanks 🙏

- [Logo](https://git-scm.com/downloads/logos) - Git Logo by [Jason Long](https://twitter.com/jasonlong) is licensed under the [CC Attribution 3.0 Unported License](https://creativecommons.org/licenses/by/3.0/).
- [README](https://github.com/thepracticaldev/dev.to/blob/master/README.md) - Thanks [dev.to](https://dev.to)! We loved their README so we made us like them!

## License 💼

This program is free software: you can redistribute it and/or modify it under the terms of the **MIT License**. Please see the [LICENSE](LICENSE) file in our repository for the full text.

##### Happy Coding ❤️
