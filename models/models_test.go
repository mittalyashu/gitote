// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package models

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func Test_parsePostgreSQLHostPort(t *testing.T) {
	testSuites := []struct {
		input      string
		host, port string
	}{
		{"127.0.0.1:1234", "127.0.0.1", "1234"},
		{"127.0.0.1", "127.0.0.1", "5432"},
		{"[::1]:1234", "[::1]", "1234"},
		{"[::1]", "[::1]", "5432"},
		{"/tmp/pg.sock:1234", "/tmp/pg.sock", "1234"},
		{"/tmp/pg.sock", "/tmp/pg.sock", "5432"},
	}

	Convey("Parse PostgreSQL host and port", t, func() {
		for _, suite := range testSuites {
			host, port := parsePostgreSQLHostPort(suite.input)
			So(host, ShouldEqual, suite.host)
			So(port, ShouldEqual, suite.port)
		}
	})
}
