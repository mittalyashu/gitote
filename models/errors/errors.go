// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package errors

import "errors"

// ErrInternalServer stores default error
var ErrInternalServer = errors.New("internal server error")

// New is a wrapper of real errors.New function.
func New(text string) error {
	return errors.New(text)
}
