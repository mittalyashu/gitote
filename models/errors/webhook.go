// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package errors

import "fmt"

// WebhookNotExist represents a "WebhookNotExist" kind of error
type WebhookNotExist struct {
	ID int64
}

// IsWebhookNotExist checks if an error is a WebhookNotExist
func IsWebhookNotExist(err error) bool {
	_, ok := err.(WebhookNotExist)
	return ok
}

func (err WebhookNotExist) Error() string {
	return fmt.Sprintf("webhook does not exist [id: %d]", err.ID)
}

// HookTaskNotExist represents a "HookTaskNotExist" kind of error
type HookTaskNotExist struct {
	HookID int64
	UUID   string
}

// IsHookTaskNotExist checks if an error is a HookTaskNotExist
func IsHookTaskNotExist(err error) bool {
	_, ok := err.(HookTaskNotExist)
	return ok
}

func (err HookTaskNotExist) Error() string {
	return fmt.Sprintf("hook task does not exist [hook_id: %d, uuid: %s]", err.HookID, err.UUID)
}
