// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package models

import (
	"fmt"
	"gitote/gitote/pkg/mailer"
	"gitote/gitote/pkg/markup"
	"gitote/gitote/pkg/setting"

	raven "github.com/getsentry/raven-go"
	"gitlab.com/gitote/com"
	log "gopkg.in/clog.v1"
)

// MailSubject returns subject of mail.
func (issue *Issue) MailSubject() string {
	return fmt.Sprintf("[%s] %s (#%d)", issue.Repo.Name, issue.Title, issue.Index)
}

// mailerUser is a wrapper for satisfying mailer.User interface.
type mailerUser struct {
	user *User
}

func (t mailerUser) ID() int64 {
	return t.user.ID
}

func (t mailerUser) DisplayName() string {
	return t.user.DisplayName()
}

func (t mailerUser) Email() string {
	return t.user.Email
}

func (t mailerUser) GenerateActivateCode() string {
	return t.user.GenerateActivateCode()
}

func (t mailerUser) GenerateEmailActivateCode(email string) string {
	return t.user.GenerateEmailActivateCode(email)
}

// NewMailerUser returns mailerUser
func NewMailerUser(u *User) mailer.User {
	return mailerUser{u}
}

// mailerRepo is a wrapper for satisfying mailer.Repository interface.
type mailerRepo struct {
	repo *Repository
}

func (t mailerRepo) FullName() string {
	return t.repo.FullName()
}

func (t mailerRepo) HTMLURL() string {
	return t.repo.HTMLURL()
}

func (t mailerRepo) ComposeMetas() map[string]string {
	return t.repo.ComposeMetas()
}

// NewMailerRepo returns mailerRepo
func NewMailerRepo(repo *Repository) mailer.Repository {
	return mailerRepo{repo}
}

// mailerIssue is a wrapper for satisfying mailer.Issue interface.
type mailerIssue struct {
	issue *Issue
}

func (t mailerIssue) MailSubject() string {
	return t.issue.MailSubject()
}

func (t mailerIssue) Content() string {
	return t.issue.Content
}

func (t mailerIssue) HTMLURL() string {
	return t.issue.HTMLURL()
}

// NewMailerIssue returns mailerIssue
func NewMailerIssue(issue *Issue) mailer.Issue {
	return mailerIssue{issue}
}

// mailIssueCommentToParticipants can be used for both new issue creation and comment.
// This functions sends two list of emails:
// 1. Repository watchers, users who participated in comments and the assignee.
// 2. Users who are not in 1. but get mentioned in current issue/comment.
func mailIssueCommentToParticipants(issue *Issue, doer *User, mentions []string) error {
	if !setting.Service.EnableNotifyMail {
		return nil
	}

	watchers, err := GetWatchers(issue.RepoID)
	if err != nil {
		return fmt.Errorf("GetWatchers [repo_id: %d]: %v", issue.RepoID, err)
	}
	participants, err := GetParticipantsByIssueID(issue.ID)
	if err != nil {
		return fmt.Errorf("GetParticipantsByIssueID [issue_id: %d]: %v", issue.ID, err)
	}

	// In case the issue poster is not watching the repository,
	// even if we have duplicated in watchers, can be safely filtered out.
	if issue.PosterID != doer.ID {
		participants = append(participants, issue.Poster)
	}

	tos := make([]string, 0, len(watchers)) // List of email addresses
	names := make([]string, 0, len(watchers))
	for i := range watchers {
		if watchers[i].UserID == doer.ID {
			continue
		}

		to, err := GetUserByID(watchers[i].UserID)
		if err != nil {
			return fmt.Errorf("GetUserByID [%d]: %v", watchers[i].UserID, err)
		}
		if to.IsOrganization() {
			continue
		}

		tos = append(tos, to.Email)
		names = append(names, to.Name)
	}
	for i := range participants {
		if participants[i].ID == doer.ID {
			continue
		} else if com.IsSliceContainsStr(names, participants[i].Name) {
			continue
		}

		tos = append(tos, participants[i].Email)
		names = append(names, participants[i].Name)
	}
	if issue.Assignee != nil && issue.Assignee.ID != doer.ID {
		if !com.IsSliceContainsStr(names, issue.Assignee.Name) {
			tos = append(tos, issue.Assignee.Email)
			names = append(names, issue.Assignee.Name)
		}
	}
	mailer.SendIssueCommentMail(NewMailerIssue(issue), NewMailerRepo(issue.Repo), NewMailerUser(doer), tos)

	// Mail mentioned people and exclude watchers.
	names = append(names, doer.Name)
	tos = make([]string, 0, len(mentions)) // list of user names.
	for i := range mentions {
		if com.IsSliceContainsStr(names, mentions[i]) {
			continue
		}

		tos = append(tos, mentions[i])
	}
	mailer.SendIssueMentionMail(NewMailerIssue(issue), NewMailerRepo(issue.Repo), NewMailerUser(doer), GetUserEmailsByNames(tos))
	return nil
}

// MailParticipants sends new issue thread created emails to repository watchers
// and mentioned people.
func (issue *Issue) MailParticipants() (err error) {
	mentions := markup.FindAllMentions(issue.Content)
	if err = updateIssueMentions(x, issue.ID, mentions); err != nil {
		return fmt.Errorf("UpdateIssueMentions [%d]: %v", issue.ID, err)
	}

	if err = mailIssueCommentToParticipants(issue, issue.Poster, mentions); err != nil {
		raven.CaptureErrorAndWait(err, nil)
		log.Error(2, "mailIssueCommentToParticipants: %v", err)
	}

	return nil
}
