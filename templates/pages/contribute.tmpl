<div class="pages bg">
    {{template "base/head" .}}
    <div class="pages head">
        <p class="text">🤝 Contribute to Gitote 🤝</p>
    </div>
    <div class="pages body">
        <center>
            <h2>Introduction</h2>
        </center>
        <p class="pages-small">
            We want to make it as easy as possible for Gitote users to become Gitote contributors, so we’ve 
            created this guide to help you get started. We have multiple tracks to cater to people of varying 
            experience levels.
        </p>
        <p class="pages-small">
            If you’re uncomfortable getting into open source development right away, you may want to consider 
            the Documentation track. Documentation are just as important as code, and we'd be happy to have 
            your contributions.
        </p>

        <center class="pages-heading">
            <h2>Development</h2>
        </center>

        <p class="pages-small">
            As contributors and maintainers of this project, we pledge to respect all people who contribute through 
            reporting issues, posting feature requests, updating documentation, submitting pull requests or patches, 
            and other activities.
        </p>

        <p class="pages-small">
            We are committed to making participation in this project a harassment-free experience for everyone, 
            regardless of level of experience, gender, gender identity and expression, sexual orientation, disability, 
            personal appearance, body size, race, ethnicity, age, or religion.
        </p>

        <p class="pages-small">
            Examples of unacceptable behavior by participants include the use of sexual language or imagery, derogatory 
            comments or personal attacks, trolling, public or private harassment, insults, or other unprofessional 
            conduct.
        </p>

        <p class="pages-small">
            Project maintainers have the right and responsibility to remove, edit, or reject comments, commits, code, 
            issues, and other contributions that are not aligned to this Code of Conduct. Project maintainers who do not 
            follow the Code of Conduct may be removed from the project team.
        </p>

        <p class="pages-small">
            This code of conduct applies both within project spaces and in public spaces when an individual is 
            representing the project or its community.
        </p>

        <p class="pages-small">
            Instances of abusive, harassing, or otherwise unacceptable behavior can be reported by emailing 
            <code class="code">contact@gitote.in</code>.
        </p>
        
        <p class="pages-small">
            This Code of Conduct is adapted from the <a href="http://contributor-covenant.org/">Contributor Covenant</a> version 1.1.0, available at 
            <a href="http://contributor-covenant.org/version/1/1/0/">http://contributor-covenant.org/version/1/1/0/</a>.
        </p>

        <center class="pages-heading">
            <h2>Documentation</h2>
        </center>

        <p class="pages-small">
            These instructions are for development of <a href="https://gitote.in">Gitote.in</a> specifically. Please 
            note that use of the Gitote Development Kit is currently experimental on Windows and macOS. Linux is recommended 
            for the best contribution experience.
        </p>
        <ol>
            <li class="pages-small">
                Read Gitote Development Kit</a>, see the <a href="https://gitote.in/gitote/gitote/src/master/docs/GDK.md">GDK README</a> 
                for instructions on setting it up and.                
            </li> 
            
            <li class="pages-small">Fork the Gitote project.</li> 
            <li class="pages-small">
                Choose an issue to work on. * You can find easy issues by <a href="https://gitote.in/gitote/gitote/issues?labels=13"> looking at issues labeled Accepting Pull Requests</a>

                <ul> 
                    <li class="pages-small">
                        Be sure to comment and verify no one else is working on the issue, and to make sure 
                        we’re still interested in a given contribution.
                    </li> 
                    <li class="pages-small">
                        You may also want to comment and ask for help if you’re new 
                        or if you get stuck. We’re happy to help!
                    </li>
                </ul>
            <li class="pages-small">Add the feature or fix the bug you’ve chosen to work on.</li> 
            <li class="pages-small">
                Open a pull request. The earlier you open a pull request, the sooner you can get feedback. You can
                mark it as a Work in Progress(In issue name) to signal that you’re not done yet.
            </li>
            <li class="pages-small">Make sure the test suite is passing <a href="https://gitlab.com/gitote/gitote/pipelines">here</a>.</li> 
            <li class="pages-small">
                Wait for a reviewer. You’ll likely need to change some things once the reviewer has completed a code review for 
                your pull request. You may also need multiple reviews depending on the size of the change.
            </li> 
            <li class="pages-small">Get your changes merged!</li>
        </ol> 
        <p class="pages-small">
            For more information, please see the <a href="https://developers.gitote.in">Developer Documentation.</a>
        </p>
    </div>
    {{template "base/footer" .}}
</div>
