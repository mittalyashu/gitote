<div class="pages bg">
    {{template "base/head" .}}
    <div class="pages head">
        <p class="text">Terms of Service</p>
    </div>
    <div class="pages body">
        <h1>Account Terms</h1>

        <p class="pages-small">
            <b>Short version:</b> <i>User Accounts and Organizations have different administrative controls; 
            a human must create your Account; you must be 13 or over; you must provide a valid email address; 
            and you may not have more than one free Account. You alone are responsible for your Account and 
            anything that happens while you are signed in to or using your Account. You are responsible for 
            keeping your Account secure.</i>
        </p>
        
        <h2 class="pages-heading">1. Account Controls</h2>
        
        <p class="pages-small">
            Users. Subject to these Terms, you retain ultimate administrative control over your User Account 
            and the Content within it.
        </p>
        
        <p class="pages-small">
            Organizations. The "owner" of an Organization that was created under these Terms has ultimate 
            administrative control over that Organization and the Content within it. Within the Service, an 
            owner can manage User access to the Organization’s data and projects. An Organization may have 
            multiple owners, but there must be at least one User Account designated as an owner of an 
            Organization. If you are the owner of an Organization under these Terms, we consider you responsible
            for the actions that are performed on or through that Organization.
        </p>
        
        <h2 class="pages-heading">2. Required Information</h2>
        
        <p class="pages-small">
            You must provide a valid email address in order to complete the signup process. Any other 
            information requested, such as your real name, is optional, unless you are accepting these terms
            on behalf of a legal entity (in which case we need more information about the legal entity).
        </p>
        
        <h2 class="pages-heading">3. Account Requirements</h2>
        
        <p class="pages-small">
            We have a few simple rules for User Accounts on Gitote's Service.
        </p>
        
        <p class="pages-small">
            You must be a human to create an Account. Accounts registered by "bots" or other automated methods
            are not permitted. We do permit machine accounts:
        </p>
        
        <p class="pages-small">
            A machine account is an Account set up by an individual human who accepts the Terms on behalf of 
            the Account, provides a valid email address, and is responsible for its actions. A machine account 
            is used exclusively for performing automated tasks. Multiple users may direct the actions of a machine
            account, but the owner of the Account is ultimately responsible for the machine's actions. You may 
            maintain no more than one free machine account in addition to your free User Account.
        </p>
        
        <p class="pages-small">
            One person or legal entity may maintain no more than one free Account (if you choose to control a 
            machine account as well, that's fine, but it can only be used for running a machine).
        </p>
        
        <p class="pages-small">
            You must be age 13 or older. While we are thrilled to see brilliant young coders get excited by 
            learning to program, we must comply with Indian law. Gitote does not target our Service to 
            children under 13, and we do not permit any Users under 13 on our Service. If we learn of any User 
            under the age of 13, we will terminate that User’s Account immediately. If you are a resident of 
            a country outside the India, your country’s minimum age may be older; in such a case, you 
            are responsible for complying with your country’s laws.
        </p>
        
        <p class="pages-small">
            Your login may only be used by one person — i.e., a single login may not be shared by multiple 
            people.
        </p>
        
        <h2 class="pages-heading">4. User Account Security</h2>
        
        <p class="pages-small">
            You are responsible for keeping your Account secure while you use our Service. We offer tools 
            such as two-factor authentication to help you maintain your Account's security, but the content
            of your Account and its security are up to you.
        </p>
        
        <p class="pages-small">
            You are responsible for all content posted and activity that occurs under your Account (even 
            when content is posted by others who have Accounts under your Account).
        </p>
        
        <p class="pages-small">
            You are responsible for maintaining the security of your Account and password. Gitote cannot
            and will not be liable for any loss or damage from your failure to comply with this security 
            obligation.
        </p>
        
        <p class="pages-small">
            You will promptly notify Gitote if you become aware of any unauthorized use of, or access to, our
            Service through your Account, including any unauthorized use of your password or Account.
        </p>
        
        <h2 class="pages-heading">5. Additional Terms</h2>
        
        <p class="pages-small">
            In some situations, third parties' terms may apply to your use of Gitote. For example, you may be 
            a member of an organization on Gitote with its own terms or license agreements; you may download an 
            application that integrates with Gitote; or you may use Gitote to authenticate to another service. 
            Please be aware that while these Terms are our full agreement with you, other parties' terms govern 
            their relationships with you.
        </p>
        
        <p class="pages-small">
            If you are a government User or otherwise accessing or using any Gitote Service in a government 
            capacity, this Government Amendment to Gitote Terms of Service applies to you, and you agree to its
            provisions.
        </p>
        
        <p class="pages-small">
            <b>Effective</b>: November 5, 2018
        </p>

        </div>
    {{template "base/footer" .}}
</div>
