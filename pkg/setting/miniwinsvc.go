// +build miniwinsvc

// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package setting

import (
	_ "gitlab.com/gitote/minwinsvc"
)

func init() {
	SupportMiniWinService = true
}
