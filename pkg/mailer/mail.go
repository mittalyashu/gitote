// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package mailer

import (
	"fmt"
	"gitote/gitote/pkg/markup"
	"gitote/gitote/pkg/setting"
	"html/template"

	raven "github.com/getsentry/raven-go"
	log "gopkg.in/clog.v1"
	"gopkg.in/gomail.v2"
	"gopkg.in/macaron.v1"
)

// Stores mail templates
const (
	MailAuthActivate       = "auth/activate"
	MailAuthActivateEmail  = "auth/activate_email"
	MailAuthResetPassword  = "auth/reset_passwd"
	MailAuthRegisterNotify = "auth/register_notify"
	MailIssueComment       = "issue/comment"
	MailIssueMention       = "issue/mention"
	MailNotifyCollaborator = "notify/collaborator"
)

// MailRender holds MailRender
type MailRender interface {
	HTMLString(string, interface{}, ...macaron.HTMLOptions) (string, error)
}

var mailRender MailRender

// InitMailRender initializes the macaron mail renderer
func InitMailRender(dir, appendDir string, funcMap []template.FuncMap) {
	opt := &macaron.RenderOptions{
		Directory:         dir,
		AppendDirectories: []string{appendDir},
		Funcs:             funcMap,
		Extensions:        []string{".tmpl", ".html"},
	}
	ts := macaron.NewTemplateSet()
	ts.Set(macaron.DEFAULT_TPL_SET_NAME, opt)

	mailRender = &macaron.TplRender{
		TemplateSet: ts,
		Opt:         opt,
	}
}

// SendTestMail sends a test mail
func SendTestMail(email string) error {
	return gomail.Send(&Sender{}, NewMessage([]string{email}, "Gitote Test Email!", "Gitote Test Email!").Message)
}

// User holds user details
type User interface {
	ID() int64
	DisplayName() string
	Email() string
	GenerateActivateCode() string
	GenerateEmailActivateCode(string) string
}

// Repository holds repository details
type Repository interface {
	FullName() string
	HTMLURL() string
	ComposeMetas() map[string]string
}

// Issue holds issue details
type Issue interface {
	MailSubject() string
	Content() string
	HTMLURL() string
}

// SendUserMail sends a mail to the user
func SendUserMail(c *macaron.Context, u User, tpl, code, subject, info string) {
	data := map[string]interface{}{
		"Username":          u.DisplayName(),
		"ActiveCodeLives":   setting.Service.ActiveCodeLives / 60,
		"ResetPwdCodeLives": setting.Service.ResetPwdCodeLives / 60,
		"Code":              code,
	}
	body, err := mailRender.HTMLString(string(tpl), data)
	if err != nil {
		raven.CaptureErrorAndWait(err, nil)
		log.Error(2, "HTMLString: %v", err)
		return
	}

	msg := NewMessage([]string{u.Email()}, subject, body)
	msg.Info = fmt.Sprintf("UID: %d, %s", u.ID(), info)

	Send(msg)
}

// SendActivateAccountMail sends an activation mail to the user (new user registration)
func SendActivateAccountMail(c *macaron.Context, u User) {
	SendUserMail(c, u, MailAuthActivate, u.GenerateActivateCode(), c.Tr("mail.activate_account"), "activate account")
}

// SendResetPasswordMail sends a password reset mail to the user
func SendResetPasswordMail(c *macaron.Context, u User) {
	SendUserMail(c, u, MailAuthResetPassword, u.GenerateActivateCode(), c.Tr("mail.reset_password"), "reset password")
}

// SendActivateEmailMail sends confirmation email.
func SendActivateEmailMail(c *macaron.Context, u User, email string) {
	data := map[string]interface{}{
		"Username":        u.DisplayName(),
		"ActiveCodeLives": setting.Service.ActiveCodeLives / 60,
		"Code":            u.GenerateEmailActivateCode(email),
		"Email":           email,
	}
	body, err := mailRender.HTMLString(string(MailAuthActivateEmail), data)
	if err != nil {
		raven.CaptureErrorAndWait(err, nil)
		log.Error(3, "HTMLString: %v", err)
		return
	}

	msg := NewMessage([]string{email}, c.Tr("mail.activate_email"), body)
	msg.Info = fmt.Sprintf("UID: %d, activate email", u.ID())

	Send(msg)
}

// SendRegisterNotifyMail triggers a notify e-mail by admin created a account.
func SendRegisterNotifyMail(c *macaron.Context, u User) {
	data := map[string]interface{}{
		"Username": u.DisplayName(),
	}
	body, err := mailRender.HTMLString(string(MailAuthRegisterNotify), data)
	if err != nil {
		raven.CaptureErrorAndWait(err, nil)
		log.Error(3, "HTMLString: %v", err)
		return
	}

	msg := NewMessage([]string{u.Email()}, c.Tr("mail.register_notify"), body)
	msg.Info = fmt.Sprintf("UID: %d, registration notify", u.ID())

	Send(msg)
}

// SendCollaboratorMail sends mail notification to new collaborator.
func SendCollaboratorMail(u, doer User, repo Repository) {
	subject := fmt.Sprintf("%s added you to %s", doer.DisplayName(), repo.FullName())

	data := map[string]interface{}{
		"Subject":  subject,
		"RepoName": repo.FullName(),
		"Link":     repo.HTMLURL(),
	}
	body, err := mailRender.HTMLString(string(MailNotifyCollaborator), data)
	if err != nil {
		raven.CaptureErrorAndWait(err, nil)
		log.Error(3, "HTMLString: %v", err)
		return
	}

	msg := NewMessage([]string{u.Email()}, subject, body)
	msg.Info = fmt.Sprintf("UID: %d, add collaborator", u.ID())

	Send(msg)
}

func composeTplData(subject, body, link string) map[string]interface{} {
	data := make(map[string]interface{}, 10)
	data["Subject"] = subject
	data["Body"] = body
	data["Link"] = link
	return data
}

func composeIssueMessage(issue Issue, repo Repository, doer User, tplName string, tos []string, info string) *Message {
	subject := issue.MailSubject()
	body := string(markup.Markdown([]byte(issue.Content()), repo.HTMLURL(), repo.ComposeMetas()))
	data := composeTplData(subject, body, issue.HTMLURL())
	data["Doer"] = doer
	content, err := mailRender.HTMLString(tplName, data)
	if err != nil {
		raven.CaptureErrorAndWait(err, nil)
		log.Error(3, "HTMLString (%s): %v", tplName, err)
	}
	from := gomail.NewMessage().FormatAddress(setting.MailService.FromEmail, doer.DisplayName())
	msg := NewMessageFrom(tos, from, subject, content)
	msg.Info = fmt.Sprintf("Subject: %s, %s", subject, info)
	return msg
}

// SendIssueCommentMail composes and sends issue comment emails to target receivers.
func SendIssueCommentMail(issue Issue, repo Repository, doer User, tos []string) {
	if len(tos) == 0 {
		return
	}

	Send(composeIssueMessage(issue, repo, doer, MailIssueComment, tos, "issue comment"))
}

// SendIssueMentionMail composes and sends issue mention emails to target receivers.
func SendIssueMentionMail(issue Issue, repo Repository, doer User, tos []string) {
	if len(tos) == 0 {
		return
	}
	Send(composeIssueMessage(issue, repo, doer, MailIssueMention, tos, "issue mention"))
}
