// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package markup

import (
	"path/filepath"
	"strings"

	"github.com/chaseadamsio/goorgeous"
	log "gopkg.in/clog.v1"
)

var orgModeExtensions = []string{".org"}

// IsOrgModeFile reports whether name looks like a Org-mode file based on its extension.
func IsOrgModeFile(name string) bool {
	extension := strings.ToLower(filepath.Ext(name))
	for _, ext := range orgModeExtensions {
		if strings.ToLower(ext) == extension {
			return true
		}
	}
	return false
}

// RawOrgMode renders content in Org-mode syntax to HTML without handling special links.
func RawOrgMode(body []byte, urlPrefix string) (result []byte) {
	// TODO: remove recover code once the third-party package is stable
	defer func() {
		if err := recover(); err != nil {
			result = body
			log.Warn("PANIC (RawOrgMode): %v", err)
		}
	}()
	return goorgeous.OrgCommon(body)
}

// OrgMode takes a string or []byte and renders to HTML in Org-mode syntax with special links.
func OrgMode(input interface{}, urlPrefix string, metas map[string]string) []byte {
	return Render(OrgModeMP, input, urlPrefix, metas)
}
