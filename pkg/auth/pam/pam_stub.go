// +build !pam

// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package pam

import (
	"errors"
)

// Auth not supported lack of pam tag
func Auth(serviceName, userName, passwd string) error {
	return errors.New("PAM not supported")
}
