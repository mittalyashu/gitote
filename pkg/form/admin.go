// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package form

import (
	"github.com/go-macaron/binding"
	"gopkg.in/macaron.v1"
)

// AdminCreateUser form for admin to create user
type AdminCreateUser struct {
	LoginType  string `binding:"Required"`
	LoginName  string
	UserName   string `binding:"Required;AlphaDashDot;MaxSize(35)"`
	Email      string `binding:"Required;Email;MaxSize(254)"`
	Password   string `binding:"MaxSize(255)"`
	SendNotify bool
}

// Validate validates form fields
func (f *AdminCreateUser) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, f, ctx.Locale)
}

// AdminEditUser form for admin to create user
type AdminEditUser struct {
	LoginType        string `binding:"Required"`
	LoginName        string
	FullName         string `binding:"MaxSize(100)"`
	Company          string
	Description      string
	ThemeColor       string
	Email            string `binding:"Required;Email;MaxSize(254)"`
	Password         string `binding:"MaxSize(255)"`
	Website          string `binding:"MaxSize(50)"`
	Location         string `binding:"MaxSize(50)"`
	Status           string `binding:"MaxSize(50)"`
	StaffNotes       string `binding:"MaxSize(255)"`
	Twitter          string `binding:"MaxSize(50)"`
	Linkedin         string `binding:"MaxSize(50)"`
	Github           string `binding:"MaxSize(50)"`
	Makerlog         string `binding:"MaxSize(50)"`
	Stackoverflow    string `binding:"MaxSize(50)"`
	Twitch           string `binding:"MaxSize(50)"`
	Telegram         string `binding:"MaxSize(50)"`
	Codepen          string `binding:"MaxSize(50)"`
	Gitlab           string `binding:"MaxSize(50)"`
	Recognized       string
	Certified        string
	MaxRepoCreation  int
	Active           bool
	Admin            bool
	AllowGitHook     bool
	AllowImportLocal bool
	Suspended        bool
	IsVerified       bool
	IsMaker          bool
	IsBugHunter      bool
	GitoteDeveloper  bool
	PrivateProfile   bool
	IsBeta           bool
	IsStaff          bool
	IsIntern         bool
}

// Validate validates form fields
func (f *AdminEditUser) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, f, ctx.Locale)
}
