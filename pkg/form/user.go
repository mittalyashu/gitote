// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package form

import (
	"mime/multipart"

	"github.com/go-macaron/binding"
	"gopkg.in/macaron.v1"
)

// Install form for installation page
type Install struct {
	DbType   string `binding:"Required"`
	DbHost   string
	DbUser   string
	DbPasswd string
	DbName   string
	SSLMode  string
	DbPath   string

	RepoRootPath        string `binding:"Required"`
	RunUser             string `binding:"Required"`
	Domain              string `binding:"Required"`
	SSHPort             int
	UseBuiltinSSHServer bool
	HTTPPort            string `binding:"Required"`
	AppURL              string `binding:"Required"`
	LogRootPath         string `binding:"Required"`
	EnableConsoleMode   bool

	SMTPHost        string
	SMTPFrom        string
	SMTPUser        string `binding:"OmitEmpty;MaxSize(254)" locale:"install.mailer_user"`
	SMTPPasswd      string
	RegisterConfirm bool
	MailNotify      bool

	OfflineMode           bool
	DisableGravatar       bool
	EnableFederatedAvatar bool
	DisableRegistration   bool
	EnableCaptcha         bool
	RequireSignInView     bool

	AdminName          string `binding:"OmitEmpty;AlphaDashDot;MaxSize(30)" locale:"install.admin_name"`
	AdminPasswd        string `binding:"OmitEmpty;MaxSize(255)" locale:"install.admin_password"`
	AdminConfirmPasswd string
	AdminEmail         string `binding:"OmitEmpty;MinSize(3);MaxSize(254);Include(@)" locale:"install.admin_email"`
}

// Validate validates fields
func (f *Install) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, f, ctx.Locale)
}

// Register form for registering
type Register struct {
	UserName string `binding:"Required;AlphaDashDot;MaxSize(35)"`
	Email    string `binding:"Required;Email;MaxSize(254)"`
	Password string `binding:"Required;MaxSize(255)"`
	Retype   string
}

// Validate validates fields
func (f *Register) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, f, ctx.Locale)
}

// SignIn form for signing in with user/password
type SignIn struct {
	UserName    string `binding:"Required;MaxSize(254)"`
	Password    string `binding:"Required;MaxSize(255)"`
	LoginSource int64
	Remember    bool
}

// Validate validates fields
func (f *SignIn) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, f, ctx.Locale)
}

// UpdateProfile form for updating profile
type UpdateProfile struct {
	Name           string `binding:"Required;AlphaDashDot;MaxSize(35)"`
	FullName       string `binding:"MaxSize(100)"`
	Company        string
	Description    string `binding:"MaxSize(255)"`
	Email          string `binding:"Required;Email;MaxSize(254)"`
	Website        string `binding:"Url;MaxSize(100)"`
	Location       string `binding:"MaxSize(50)"`
	Status         string `binding:"MaxSize(25)"`
	ThemeColor     string
	IsBeta         bool
	ShowAds        bool
	PrivateEmail   bool
	PrivateProfile bool
}

// UpdateSocial form for updating social
type UpdateSocial struct {
	Twitter       string `binding:"MaxSize(50)"`
	Linkedin      string `binding:"MaxSize(50)"`
	Github        string `binding:"MaxSize(50)"`
	Makerlog      string `binding:"MaxSize(50)"`
	Stackoverflow string `binding:"MaxSize(50)"`
	Twitch        string `binding:"MaxSize(50)"`
	Telegram      string `binding:"MaxSize(50)"`
	Codepen       string `binding:"MaxSize(50)"`
	Gitlab        string `binding:"MaxSize(50)"`
}

// Validate validates fields
func (f *UpdateProfile) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, f, ctx.Locale)
}

// Avatar types
const (
	AvatarLocal  string = "local"
	AvatarByMail string = "bymail"
)

// Avatar form for changing avatar
type Avatar struct {
	Source      string
	Avatar      *multipart.FileHeader
	Gravatar    string `binding:"OmitEmpty;Email;MaxSize(254)"`
	Federavatar bool
}

// Validate validates fields
func (f *Avatar) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, f, ctx.Locale)
}

// AddEmail form for adding new email
type AddEmail struct {
	Email string `binding:"Required;Email;MaxSize(254)"`
}

// Validate validates fields
func (f *AddEmail) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, f, ctx.Locale)
}

// ChangePassword form for changing password
type ChangePassword struct {
	OldPassword string `binding:"Required;MinSize(1);MaxSize(255)"`
	Password    string `binding:"Required;MaxSize(255)"`
	Retype      string
}

// Validate validates fields
func (f *ChangePassword) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, f, ctx.Locale)
}

// AddSSHKey form for adding SSH key
type AddSSHKey struct {
	Title   string `binding:"Required;MaxSize(50)"`
	Content string `binding:"Required"`
}

// Validate validates fields
func (f *AddSSHKey) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, f, ctx.Locale)
}

// NewAccessToken form for creating access token
type NewAccessToken struct {
	Name string `binding:"Required"`
}

// Validate validates fields
func (f *NewAccessToken) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, f, ctx.Locale)
}
