// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package avatar

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func Test_RandomImage(t *testing.T) {
	Convey("Generate a random avatar from email", t, func() {
		_, err := RandomImage([]byte("gitote@local"))
		So(err, ShouldBeNil)

		Convey("Try to generate an image with size zero", func() {
			_, err := RandomImageSize(0, []byte("gitote@local"))
			So(err, ShouldNotBeNil)
		})
	})
}
