// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package tool

import (
	"path/filepath"
	"strings"
)

// IsSameSiteURLPath returns true if the URL path belongs to the same site, false otherwise.
// False: //url, http://url, /\url
// True: /url
func IsSameSiteURLPath(url string) bool {
	return len(url) >= 2 && url[0] == '/' && url[1] != '/' && url[1] != '\\'
}

// IsMaliciousPath returns true if given path is an absolute path or contains malicious content
// which has potential to traverse upper level directories.
func IsMaliciousPath(path string) bool {
	return filepath.IsAbs(path) || strings.Contains(path, "..")
}
