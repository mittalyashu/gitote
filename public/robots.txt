# If you would like to crawl Gitote contact us at support@gitote.in.

#
#
#              _____
#             |     |
#             | | | |
#             |_____|
#       ____ ___|_|___ ____
#      ()___)         ()___)
#      // /|           |\ \\
#     // / |           | \ \\
#    (___) |___________| (___)
#    (___)   (_______)   (___)
#    (___)     (___)     (___)
#    (___)      |_|      (___)
#    (___)  ___/___\___   | |
#     | |  |           |  | |
#     | |  |___________| /___\ 
#    /___\  |||     ||| //   \\
#   //   \\ |||     ||| \\   //
#   \\   // |||     |||  \\ //
#    \\ // ()__)   (__()
#          ///       \\\ 
#         ///         \\\ 
#       _///___     ___\\\_
#      |_______|   |_______|
#
#
#

User-agent: Googlebot
Allow: /humans.txt
Allow: /
Allow: /login
Allow: /join
Allow: /*/*/src/master
Disallow: /api/*
Disallow: /admin
Disallow: /avatars
Disallow: /user
Disallow: /pulls
Disallow: /issues
Disallow: /repo
Disallow: /org
Disallow: /sitemap
Disallow: /reset_password
Disallow: /*/*/forks
Disallow: /*/*/stars
Disallow: /*/*/pulls
Disallow: /*/*/archive
Disallow: /*/*/settings
Disallow: /*/*/raw/*
Disallow: /*/*/wiki/_new
Disallow: /*/*/wiki/*/_edit
Disallow: /*/*/commit
Disallow: /*/*/src/*
Disallow: /*/*/_edit
Disallow: /*/*/issues/new
Disallow: /*/*/issues?
Disallow: /*/*/compare

Sitemap: https://gitote.in/sitemap
Sitemap: https://gitote.in/sitemap/users
Sitemap: https://gitote.in/sitemap/orgs
Sitemap: https://gitote.in/sitemap/repos
