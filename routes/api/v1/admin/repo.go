// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package admin

import (
	"gitote/gitote/pkg/context"
	"gitote/gitote/routes/api/v1/repo"
	"gitote/gitote/routes/api/v1/user"

	api "gitlab.com/gitote/go-gitote-client"
)

// CreateRepo api for creating a repository
func CreateRepo(c *context.APIContext, form api.CreateRepoOption) {
	owner := user.GetUserByParams(c)
	if c.Written() {
		return
	}

	repo.CreateUserRepo(c, owner, form)
}
