// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package admin

import (
	"gitote/gitote/pkg/context"
	"gitote/gitote/routes/api/v1/org"
	"gitote/gitote/routes/api/v1/user"

	api "gitlab.com/gitote/go-gitote-client"
)

// CreateOrg api for create organization
func CreateOrg(c *context.APIContext, form api.CreateOrgOption) {
	org.CreateOrgForUser(c, form, user.GetUserByParams(c))
}
