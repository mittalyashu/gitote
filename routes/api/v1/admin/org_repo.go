// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package admin

import (
	"gitote/gitote/models"
	"gitote/gitote/models/errors"
	"gitote/gitote/pkg/context"
)

// GetRepositoryByParams gets repository with params
func GetRepositoryByParams(c *context.APIContext) *models.Repository {
	repo, err := models.GetRepositoryByName(c.Org.Team.OrgID, c.Params(":reponame"))
	if err != nil {
		if errors.IsRepoNotExist(err) {
			c.Status(404)
		} else {
			c.Error(500, "GetRepositoryByName", err)
		}
		return nil
	}
	return repo
}

// AddTeamRepository api for adding a repository
func AddTeamRepository(c *context.APIContext) {
	repo := GetRepositoryByParams(c)
	if c.Written() {
		return
	}
	if err := c.Org.Team.AddRepository(repo); err != nil {
		c.Error(500, "AddRepository", err)
		return
	}

	c.Status(204)
}

// RemoveTeamRepository api for removing a repository
func RemoveTeamRepository(c *context.APIContext) {
	repo := GetRepositoryByParams(c)
	if c.Written() {
		return
	}
	if err := c.Org.Team.RemoveRepository(repo.ID); err != nil {
		c.Error(500, "RemoveRepository", err)
		return
	}

	c.Status(204)
}
