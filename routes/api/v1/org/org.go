// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package org

import (
	"gitote/gitote/models"
	"gitote/gitote/pkg/context"
	"gitote/gitote/routes/api/v1/convert"
	"gitote/gitote/routes/api/v1/user"

	api "gitlab.com/gitote/go-gitote-client"
)

// CreateOrgForUser api for create organization
func CreateOrgForUser(c *context.APIContext, apiForm api.CreateOrgOption, user *models.User) {
	if c.Written() {
		return
	}

	org := &models.User{
		Name:        apiForm.UserName,
		FullName:    apiForm.FullName,
		Description: apiForm.Description,
		Website:     apiForm.Website,
		Location:    apiForm.Location,
		IsActive:    true,
		Type:        models.UserTypeOrganization,
	}
	if err := models.CreateOrganization(org, user); err != nil {
		if models.IsErrUserAlreadyExist(err) ||
			models.IsErrNameReserved(err) ||
			models.IsErrNamePatternNotAllowed(err) {
			c.Error(422, "", err)
		} else {
			c.Error(500, "CreateOrganization", err)
		}
		return
	}

	c.JSON(201, convert.ToOrganization(org))
}

func listUserOrgs(c *context.APIContext, u *models.User, all bool) {
	if err := u.GetOrganizations(all); err != nil {
		c.Error(500, "GetOrganizations", err)
		return
	}

	apiOrgs := make([]*api.Organization, len(u.Orgs))
	for i := range u.Orgs {
		apiOrgs[i] = convert.ToOrganization(u.Orgs[i])
	}
	c.JSON(200, &apiOrgs)
}

// ListMyOrgs list all my orgs
func ListMyOrgs(c *context.APIContext) {
	listUserOrgs(c, c.User, true)
}

// CreateMyOrg list user's orgs
func CreateMyOrg(c *context.APIContext, apiForm api.CreateOrgOption) {
	CreateOrgForUser(c, apiForm, c.User)
}

// ListUserOrgs list user's orgs
func ListUserOrgs(c *context.APIContext) {
	u := user.GetUserByParams(c)
	if c.Written() {
		return
	}
	listUserOrgs(c, u, false)
}

// Get get an organization
func Get(c *context.APIContext) {
	c.JSON(200, convert.ToOrganization(c.Org.Organization))
}

// Edit change an organization's information
func Edit(c *context.APIContext, form api.EditOrgOption) {
	org := c.Org.Organization
	if !org.IsOwnedBy(c.User.ID) {
		c.Status(403)
		return
	}

	org.FullName = form.FullName
	org.Description = form.Description
	org.Website = form.Website
	org.Location = form.Location
	if err := models.UpdateUser(org); err != nil {
		c.Error(500, "UpdateUser", err)
		return
	}

	c.JSON(200, convert.ToOrganization(org))
}
