// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package user

import (
	"gitote/gitote/models"
	"gitote/gitote/pkg/context"

	api "gitlab.com/gitote/go-gitote-client"
)

// ListAccessTokens list all the access tokens
func ListAccessTokens(c *context.APIContext) {
	tokens, err := models.ListAccessTokens(c.User.ID)
	if err != nil {
		c.Error(500, "ListAccessTokens", err)
		return
	}

	apiTokens := make([]*api.AccessToken, len(tokens))
	for i := range tokens {
		apiTokens[i] = &api.AccessToken{tokens[i].Name, tokens[i].Sha1}
	}
	c.JSON(200, &apiTokens)
}

// CreateAccessToken create access tokens
func CreateAccessToken(c *context.APIContext, form api.CreateAccessTokenOption) {
	t := &models.AccessToken{
		UID:  c.User.ID,
		Name: form.Name,
	}
	if err := models.NewAccessToken(t); err != nil {
		c.Error(500, "NewAccessToken", err)
		return
	}
	c.JSON(201, &api.AccessToken{t.Name, t.Sha1})
}
