// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package repo

import (
	"gitote/gitote/models"
	"gitote/gitote/models/errors"
	"gitote/gitote/pkg/context"

	api "gitlab.com/gitote/go-gitote-client"
)

// ListCollaborators list a repository's collaborators
func ListCollaborators(c *context.APIContext) {
	collaborators, err := c.Repo.Repository.GetCollaborators()
	if err != nil {
		c.ServerError("GetCollaborators", err)
	}

	apiCollaborators := make([]*api.Collaborator, len(collaborators))
	for i := range collaborators {
		apiCollaborators[i] = collaborators[i].APIFormat()
	}
	c.JSONSuccess(&apiCollaborators)
}

// AddCollaborator add a collaborator to a repository
func AddCollaborator(c *context.APIContext, form api.AddCollaboratorOption) {
	collaborator, err := models.GetUserByName(c.Params(":collaborator"))
	if err != nil {
		if errors.IsUserNotExist(err) {
			c.Error(422, "", err)
		} else {
			c.Error(500, "GetUserByName", err)
		}
		return
	}

	if err := c.Repo.Repository.AddCollaborator(collaborator); err != nil {
		c.Error(500, "AddCollaborator", err)
		return
	}

	if form.Permission != nil {
		if err := c.Repo.Repository.ChangeCollaborationAccessMode(collaborator.ID, models.ParseAccessMode(*form.Permission)); err != nil {
			c.Error(500, "ChangeCollaborationAccessMode", err)
			return
		}
	}

	c.Status(204)
}

// IsCollaborator check if a user is a collaborator of a repository
func IsCollaborator(c *context.APIContext) {
	collaborator, err := models.GetUserByName(c.Params(":collaborator"))
	if err != nil {
		if errors.IsUserNotExist(err) {
			c.Error(422, "", err)
		} else {
			c.Error(500, "GetUserByName", err)
		}
		return
	}

	if !c.Repo.Repository.IsCollaborator(collaborator.ID) {
		c.Status(404)
	} else {
		c.Status(204)
	}
}

// DeleteCollaborator delete a collaborator from a repository
func DeleteCollaborator(c *context.APIContext) {
	collaborator, err := models.GetUserByName(c.Params(":collaborator"))
	if err != nil {
		if errors.IsUserNotExist(err) {
			c.Error(422, "", err)
		} else {
			c.Error(500, "GetUserByName", err)
		}
		return
	}

	if err := c.Repo.Repository.DeleteCollaboration(collaborator.ID); err != nil {
		c.Error(500, "DeleteCollaboration", err)
		return
	}

	c.Status(204)
}
