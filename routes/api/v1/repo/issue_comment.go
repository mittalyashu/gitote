// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package repo

import (
	"gitote/gitote/models"
	"gitote/gitote/pkg/context"
	"time"

	api "gitlab.com/gitote/go-gitote-client"
)

// ListIssueComments list all the comments of an issue
func ListIssueComments(c *context.APIContext) {
	var since time.Time
	if len(c.Query("since")) > 0 {
		var err error
		since, err = time.Parse(time.RFC3339, c.Query("since"))
		if err != nil {
			c.Error(422, "", err)
			return
		}
	}

	// comments,err:=models.GetCommentsByIssueIDSince(, since)
	issue, err := models.GetRawIssueByIndex(c.Repo.Repository.ID, c.ParamsInt64(":index"))
	if err != nil {
		c.Error(500, "GetRawIssueByIndex", err)
		return
	}

	comments, err := models.GetCommentsByIssueIDSince(issue.ID, since.Unix())
	if err != nil {
		c.Error(500, "GetCommentsByIssueIDSince", err)
		return
	}

	apiComments := make([]*api.Comment, len(comments))
	for i := range comments {
		apiComments[i] = comments[i].APIFormat()
	}
	c.JSON(200, &apiComments)
}

// ListRepoIssueComments returns all issue-comments for a repo
func ListRepoIssueComments(c *context.APIContext) {
	var since time.Time
	if len(c.Query("since")) > 0 {
		var err error
		since, err = time.Parse(time.RFC3339, c.Query("since"))
		if err != nil {
			c.Error(422, "", err)
			return
		}
	}

	comments, err := models.GetCommentsByRepoIDSince(c.Repo.Repository.ID, since.Unix())
	if err != nil {
		c.Error(500, "GetCommentsByRepoIDSince", err)
		return
	}

	apiComments := make([]*api.Comment, len(comments))
	for i := range comments {
		apiComments[i] = comments[i].APIFormat()
	}
	c.JSON(200, &apiComments)
}

// CreateIssueComment create a comment for an issue
func CreateIssueComment(c *context.APIContext, form api.CreateIssueCommentOption) {
	issue, err := models.GetIssueByIndex(c.Repo.Repository.ID, c.ParamsInt64(":index"))
	if err != nil {
		c.Error(500, "GetIssueByIndex", err)
		return
	}

	comment, err := models.CreateIssueComment(c.User, c.Repo.Repository, issue, form.Body, nil)
	if err != nil {
		c.Error(500, "CreateIssueComment", err)
		return
	}

	c.JSON(201, comment.APIFormat())
}

// EditIssueComment modify a comment of an issue
func EditIssueComment(c *context.APIContext, form api.EditIssueCommentOption) {
	comment, err := models.GetCommentByID(c.ParamsInt64(":id"))
	if err != nil {
		if models.IsErrCommentNotExist(err) {
			c.Error(404, "GetCommentByID", err)
		} else {
			c.Error(500, "GetCommentByID", err)
		}
		return
	}

	if c.User.ID != comment.PosterID && !c.Repo.IsAdmin() {
		c.Status(403)
		return
	} else if comment.Type != models.CommentTypeComment {
		c.Status(204)
		return
	}

	oldContent := comment.Content
	comment.Content = form.Body
	if err := models.UpdateComment(c.User, comment, oldContent); err != nil {
		c.Error(500, "UpdateComment", err)
		return
	}
	c.JSON(200, comment.APIFormat())
}

// DeleteIssueComment delete a comment from an issue
func DeleteIssueComment(c *context.APIContext) {
	comment, err := models.GetCommentByID(c.ParamsInt64(":id"))
	if err != nil {
		if models.IsErrCommentNotExist(err) {
			c.Error(404, "GetCommentByID", err)
		} else {
			c.Error(500, "GetCommentByID", err)
		}
		return
	}

	if c.User.ID != comment.PosterID && !c.Repo.IsAdmin() {
		c.Status(403)
		return
	} else if comment.Type != models.CommentTypeComment {
		c.Status(204)
		return
	}

	if err = models.DeleteCommentByID(c.User, comment.ID); err != nil {
		c.Error(500, "DeleteCommentByID", err)
		return
	}
	c.Status(204)
}
