// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package repo

import (
	"gitote/gitote/models"
	"gitote/gitote/pkg/context"
	"gitote/gitote/routes/repo"

	"gitlab.com/gitote/git-module"
)

// GetRawFile get a file by path on a repository
func GetRawFile(c *context.APIContext) {
	if !c.Repo.HasAccess() {
		c.Status(404)
		return
	}

	if c.Repo.Repository.IsBare {
		c.Status(404)
		return
	}

	blob, err := c.Repo.Commit.GetBlobByPath(c.Repo.TreePath)
	if err != nil {
		if git.IsErrNotExist(err) {
			c.Status(404)
		} else {
			c.Error(500, "GetBlobByPath", err)
		}
		return
	}
	if err = repo.ServeBlob(c.Context, blob); err != nil {
		c.Error(500, "ServeBlob", err)
	}
}

// GetArchive get archive of a repository
func GetArchive(c *context.APIContext) {
	repoPath := models.RepoPath(c.Params(":username"), c.Params(":reponame"))
	gitRepo, err := git.OpenRepository(repoPath)
	if err != nil {
		c.Error(500, "OpenRepository", err)
		return
	}
	c.Repo.GitRepo = gitRepo

	repo.Download(c.Context)
}

// GetEditorconfig get editor config of a repository
func GetEditorconfig(c *context.APIContext) {
	ec, err := c.Repo.GetEditorconfig()
	if err != nil {
		if git.IsErrNotExist(err) {
			c.Error(404, "GetEditorconfig", err)
		} else {
			c.Error(500, "GetEditorconfig", err)
		}
		return
	}

	fileName := c.Params("filename")
	def := ec.GetDefinitionForFilename(fileName)
	if def == nil {
		c.Error(404, "GetDefinitionForFilename", err)
		return
	}
	c.JSON(200, def)
}
