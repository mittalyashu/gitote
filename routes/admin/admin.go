// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package admin

import (
	"fmt"
	"gitote/gitote/models"
	"gitote/gitote/pkg/context"
	"gitote/gitote/pkg/cron"
	"gitote/gitote/pkg/mailer"
	"gitote/gitote/pkg/process"
	"gitote/gitote/pkg/setting"
	"gitote/gitote/pkg/tool"
	"runtime"
	"strings"
	"time"

	"github.com/json-iterator/go"
	"gitlab.com/gitote/com"
	"gopkg.in/macaron.v1"
)

const (
	// DashboardTPL page template
	DashboardTPL = "admin/dashboard"

	// AnalyticsTPL page template
	AnalyticsTPL = "admin/analytics"

	// ConfigTPL page template
	ConfigTPL = "admin/config"

	// MonitorTPL page template
	MonitorTPL = "admin/monitor"
)

var (
	startTime = time.Now()
)

var sysStatus struct {
	Uptime       string
	NumGoroutine int

	// General statistics.
	MemAllocated string // bytes allocated and still in use
	MemTotal     string // bytes allocated (even if freed)
	MemSys       string // bytes obtained from system (sum of XxxSys below)
	Lookups      uint64 // number of pointer lookups
	MemMallocs   uint64 // number of mallocs
	MemFrees     uint64 // number of frees

	// Main allocation heap statistics.
	HeapAlloc    string // bytes allocated and still in use
	HeapSys      string // bytes obtained from system
	HeapIdle     string // bytes in idle spans
	HeapInuse    string // bytes in non-idle span
	HeapReleased string // bytes released to the OS
	HeapObjects  uint64 // total number of allocated objects

	// Low-level fixed-size structure allocator statistics.
	//	Inuse is bytes used now.
	//	Sys is bytes obtained from system.
	StackInuse  string // bootstrap stacks
	StackSys    string
	MSpanInuse  string // mspan structures
	MSpanSys    string
	MCacheInuse string // mcache structures
	MCacheSys   string
	BuckHashSys string // profiling bucket hash table
	GCSys       string // GC metadata
	OtherSys    string // other system allocations

	// Garbage collector statistics.
	NextGC       string // next run in HeapAlloc time (bytes)
	LastGC       string // last run in absolute time (ns)
	PauseTotalNs string
	PauseNs      string // circular buffer of recent GC pause times, most recent at [(NumGC+255)%256]
	NumGC        uint32
}

func updateSystemStatus() {
	sysStatus.Uptime = tool.TimeSincePro(startTime)

	m := new(runtime.MemStats)
	runtime.ReadMemStats(m)
	sysStatus.NumGoroutine = runtime.NumGoroutine()

	sysStatus.MemAllocated = tool.FileSize(int64(m.Alloc))
	sysStatus.MemTotal = tool.FileSize(int64(m.TotalAlloc))
	sysStatus.MemSys = tool.FileSize(int64(m.Sys))
	sysStatus.Lookups = m.Lookups
	sysStatus.MemMallocs = m.Mallocs
	sysStatus.MemFrees = m.Frees

	sysStatus.HeapAlloc = tool.FileSize(int64(m.HeapAlloc))
	sysStatus.HeapSys = tool.FileSize(int64(m.HeapSys))
	sysStatus.HeapIdle = tool.FileSize(int64(m.HeapIdle))
	sysStatus.HeapInuse = tool.FileSize(int64(m.HeapInuse))
	sysStatus.HeapReleased = tool.FileSize(int64(m.HeapReleased))
	sysStatus.HeapObjects = m.HeapObjects

	sysStatus.StackInuse = tool.FileSize(int64(m.StackInuse))
	sysStatus.StackSys = tool.FileSize(int64(m.StackSys))
	sysStatus.MSpanInuse = tool.FileSize(int64(m.MSpanInuse))
	sysStatus.MSpanSys = tool.FileSize(int64(m.MSpanSys))
	sysStatus.MCacheInuse = tool.FileSize(int64(m.MCacheInuse))
	sysStatus.MCacheSys = tool.FileSize(int64(m.MCacheSys))
	sysStatus.BuckHashSys = tool.FileSize(int64(m.BuckHashSys))
	sysStatus.GCSys = tool.FileSize(int64(m.GCSys))
	sysStatus.OtherSys = tool.FileSize(int64(m.OtherSys))

	sysStatus.NextGC = tool.FileSize(int64(m.NextGC))
	sysStatus.LastGC = fmt.Sprintf("%.1fs", float64(time.Now().UnixNano()-int64(m.LastGC))/1000/1000/1000)
	sysStatus.PauseTotalNs = fmt.Sprintf("%.1fs", float64(m.PauseTotalNs)/1000/1000/1000)
	sysStatus.PauseNs = fmt.Sprintf("%.3fs", float64(m.PauseNs[(m.NumGC+255)%256])/1000/1000/1000)
	sysStatus.NumGC = m.NumGC
}

// AdminOperation types.
type AdminOperation int

const (
	// CleanInactiveUser operation value
	CleanInactiveUser AdminOperation = iota + 1

	// CleanRepoArchives operation value
	CleanRepoArchives

	// CleanMissingRepos operation value
	CleanMissingRepos

	// GitGCRepos operation value
	GitGCRepos

	// SyncSSHAuthorizedKey operation value
	SyncSSHAuthorizedKey

	// SyncRepositoryHooks operation value
	SyncRepositoryHooks

	// ReinitMissingRepository operation value
	ReinitMissingRepository
)

// Dashboard shows dashboard page
func Dashboard(c *context.Context) {
	c.Data["Title"] = "Dashboard"
	c.Data["PageIsAdmin"] = true
	c.Data["PageIsAdminDashboard"] = true

	// Run operation.
	op, _ := com.StrTo(c.Query("op")).Int()
	if op > 0 {
		var err error
		var success string

		switch AdminOperation(op) {
		case CleanRepoArchives:
			success = "All repositories archives have been deleted successfully."
			err = models.DeleteRepositoryArchives()
		case CleanMissingRepos:
			success = "All repository records that lost Git files have been deleted successfully."
			err = models.DeleteMissingRepositories()
		case GitGCRepos:
			success = "All repositories have done garbage collection successfully."
			err = models.GitGcRepos()
		case SyncSSHAuthorizedKey:
			success = "All public keys have been rewritten successfully."
			err = models.RewriteAuthorizedKeys()
		case SyncRepositoryHooks:
			success = "All repositories' pre-receive, update and post-receive hooks have been resynced successfully."
			err = models.SyncRepositoryHooks()
		case ReinitMissingRepository:
			success = "All repository records that lost Git files have been reinitialized successfully."
			err = models.ReinitMissingRepositories()
		}

		if err != nil {
			c.Flash.Error(err.Error())
		} else {
			c.Flash.Success(success)
		}
		c.Redirect(setting.AppSubURL + "/admin")
		return
	}

	c.Data["Stats"] = models.GetStatistic()
	// FIXME: update periodically
	updateSystemStatus()
	c.Data["SysStatus"] = sysStatus
	c.Data["GitVersion"] = setting.Git.Version
	c.HTML(200, DashboardTPL)
}

// SendTestMail send mail to verify the email
func SendTestMail(c *context.Context) {
	email := c.Query("email")
	// Send a test email to the user's email address and redirect back to Config
	if err := mailer.SendTestMail(email); err != nil {
		c.Flash.Error(c.Tr("admin.config.test_mail_failed", email, err))
	} else {
		c.Flash.Info(c.Tr("admin.config.test_mail_sent", email))
	}

	c.Redirect(setting.AppSubURL + "/admin/config")
}

// Config shows configuration page
func Config(c *context.Context) {
	c.Data["Title"] = "Configuration"
	c.Data["PageIsAdmin"] = true
	c.Data["PageIsAdminConfig"] = true

	c.Data["AppURL"] = setting.AppURL
	c.Data["Domain"] = setting.Domain
	c.Data["OfflineMode"] = setting.OfflineMode
	c.Data["DisableRouterLog"] = setting.DisableRouterLog
	c.Data["RunUser"] = setting.RunUser
	c.Data["RunMode"] = strings.Title(macaron.Env)
	c.Data["StaticRootPath"] = setting.StaticRootPath
	c.Data["LogRootPath"] = setting.LogRootPath
	c.Data["ReverseProxyAuthUser"] = setting.ReverseProxyAuthUser

	c.Data["SSH"] = setting.SSH

	c.Data["RepoRootPath"] = setting.RepoRootPath
	c.Data["ScriptType"] = setting.ScriptType
	c.Data["Repository"] = setting.Repository
	c.Data["HTTP"] = setting.HTTP

	c.Data["DbCfg"] = models.DbCfg
	c.Data["Service"] = setting.Service
	c.Data["Webhook"] = setting.Webhook

	c.Data["MailerEnabled"] = false
	if setting.MailService != nil {
		c.Data["MailerEnabled"] = true
		c.Data["Mailer"] = setting.MailService
	}

	c.Data["CacheAdapter"] = setting.CacheAdapter
	c.Data["CacheInterval"] = setting.CacheInterval
	c.Data["CacheConn"] = setting.CacheConn

	c.Data["SessionConfig"] = setting.SessionConfig

	c.Data["DisableGravatar"] = setting.DisableGravatar
	c.Data["EnableFederatedAvatar"] = setting.EnableFederatedAvatar

	c.Data["GitVersion"] = setting.Git.Version
	c.Data["Git"] = setting.Git

	type logger struct {
		Mode, Config string
	}
	loggers := make([]*logger, len(setting.LogModes))
	for i := range setting.LogModes {
		loggers[i] = &logger{
			Mode: strings.Title(setting.LogModes[i]),
		}

		result, _ := jsoniter.MarshalIndent(setting.LogConfigs[i], "", "  ")
		loggers[i].Config = string(result)
	}
	c.Data["Loggers"] = loggers

	c.HTML(200, ConfigTPL)
}

// Monitor shows monitor page
func Monitor(c *context.Context) {
	c.Data["Title"] = "Monitoring"
	c.Data["PageIsAdmin"] = true
	c.Data["PageIsAdminMonitor"] = true
	c.Data["Processes"] = process.Processes
	c.Data["Entries"] = cron.ListTasks()
	c.HTML(200, MonitorTPL)
}

// Analytics shows analytics page
func Analytics(c *context.Context) {
	c.Data["Title"] = "Analytics"
	c.Data["PageIsAdminAnalytics"] = true
	c.HTML(200, AnalyticsTPL)
}
