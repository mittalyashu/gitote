// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package admin

import (
	"gitote/gitote/models"
	"gitote/gitote/pkg/context"
	"gitote/gitote/routes"
)

const (
	// AllUsersTPL list page template
	AllUsersTPL = "admin/allusers"
)

// AllUsers shows all users page
func AllUsers(c *context.Context) {
	routes.RenderUserSearch(c, &routes.UserSearchOptions{
		Type:    models.UserTypeIndividual,
		Counter: models.CountUsers,
		Ranger:  models.Users,
		OrderBy: "id ASC",
		TplName: AllUsersTPL,
	})
}
