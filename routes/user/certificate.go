// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package user

import (
	"gitote/gitote/pkg/context"
	"strings"
)

const (
	// InterCertificateTPL page template
	InterCertificateTPL = "misc/internscert"
)

// InternCertificate returns user details for the certificate
func InternCertificate(c *context.Context) {
	ctxUser := GetUserByName(c, strings.TrimSuffix(c.Params(":username"), ""))
	c.Data["Owner"] = ctxUser

	c.HTML(200, InterCertificateTPL)
}
