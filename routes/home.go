// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package routes

import (
	"gitote/gitote/pkg/context"
	"gitote/gitote/pkg/setting"
	"gitote/gitote/routes/user"
)

const (
	// HomeTPL page template
	HomeTPL = "home"
)

// Home shows the home page
func Home(c *context.Context) {
	if c.IsLogged {
		if !c.User.IsActive && setting.Service.RegisterEmailConfirm {
			c.Data["Title"] = c.Tr("auth.active_your_account")
			c.Success(user.ActivateTPL)
		} else {
			user.Dashboard(c)
		}
		return
	}

	// Check auto-login.
	uname := c.GetCookie(setting.CookieUserName)
	if len(uname) != 0 {
		c.Redirect(setting.AppSubURL + "/login")
		return
	}

	c.Data["PageIsHome"] = true
	c.Success(HomeTPL)
}

// NotFound renders 404 page if page not found
func NotFound(c *context.Context) {
	c.Data["Title"] = "Page Not Found"
	c.NotFound()
}
