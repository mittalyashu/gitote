# The stack used in Gitote

#### Open Source

- [GoLang](https://golang.org/)
- [PostgreSQL](https://www.postgresql.org/)
- [XORM](http://xorm.io/)
- [Semantic UI](https://semantic-ui.com/)
- [GitHub Octicons](https://octicons.github.com/)
- [Font Awesome](https://fontawesome.io/)
- [jQuery](https://jquery.com/)

#### Infra/Services

- [Gitote](https://gitote.in) - VCS
- [DigitalOcean](https://m.do.co/c/1ddec8d50826) - Hosting
- [Ubuntu 18.10 x64](https://www.ubuntu.com/) - Operating System
- [NGINX](https://www.nginx.com/) - Server
- [Let's Encrypt](https://letsencrypt.org/) - SSL
- [Google Analytics](https://analytics.google.com/analytics/web/) - Web Analytics
- [GitLab CI](https://about.gitlab.com/product/continuous-integration/) - Continuous Integration
- [BrowserStack](https://www.browserstack.com) - Cross-platform Browser testing
- [jsDelivr](https://www.jsdelivr.com/) - Static Assets CDN
- [Gravatar](https://gravatar.com/) - Globally Recognized Avatar
- [Heroku Toolbelt](https://blog.heroku.com/the_heroku_toolbelt) - For development(one-click launch)

#### Ops

- [OteBot](https://gitote.in/gitote/otebot)
- [Discord](https://discord.gg/wEUXMJp)
